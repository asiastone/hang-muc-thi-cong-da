# Hạng mục thi công đá

AsiaStone nhận thi công ốp lát các loại đá hoa cương thiên nhiên, marble với nhiều hạng mục thi công như:

- Ốp đá hoa cương mặt tiền

- [ốp đá cửa thang máy](https://asiastone.vn/op-da-granite-thang-may)

- lát đá phòng khách

- thi công đá mặt bếp

- [ốp đá tường phòng khách](https://asiastone.vn/op-lat-da-granite-phong-khach)

- ốp đá bậc tam cấp

- ốp đá granite thang máy

- [mặt đá lavabo đẹp](https://asiastone.vn/thi-cong-ban-lavabo-da-tu-nhien)

Nhận báo giá tại đây: đá granite lát nền, đá hoa cương marble